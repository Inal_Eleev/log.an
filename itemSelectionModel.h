#ifndef ITEMSELECTIONMODEL_H
#define ITEMSELECTIONMODEL_H

#include <QItemSelectionModel>

class LogItemSelectionModel : public QItemSelectionModel
{
public:
	explicit LogItemSelectionModel(QAbstractItemModel *model = nullptr);
	explicit LogItemSelectionModel(QAbstractItemModel *model, QObject *parent);

	virtual ~LogItemSelectionModel();
};
#endif