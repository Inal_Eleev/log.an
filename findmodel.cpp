#include "findmodel.h"

QFindListModel::QFindListModel(QFile* file, QListModel* parent)
	: m_parent(parent)
	, m_fileq(file)
	, lastIndex(100)
	, firstIndex(0)
	, rows(0)
	, is_sorted(false)
{
}

QFindListModel::~QFindListModel()
{
}

void QFindListModel::InitFindMap(int line)
{
	offsetsMap.append(line);
	beginInsertRows(QModelIndex(), rows, offsetsMap.size());
	rows = offsetsMap.size();
	endInsertRows();
}

void QFindListModel::findFinished()
{
	is_sorted = false;
}

int QFindListModel::rowCount(const QModelIndex & /*parent*/) const
{
	return offsetsMap.size();
}

QVariant QFindListModel::data(const QModelIndex & index, int role) const
{
	if (!is_sorted)
	{
		auto sort = [](int a, int b) -> bool { return a < b; };
		qSort(offsetsMap.begin(), offsetsMap.end(), sort);
		is_sorted = true;
	}

	if (!index.isValid() || !(index.row() >= firstIndex && index.row() <= lastIndex))
		return QVariant();

	switch (role)
	{
	case Qt::EditRole:
	case Qt::DisplayRole:
	{
		try
		{
			const auto it = offsetsMap.at(index.row());
			return m_parent->GetDataByLine(it);
		}
		catch (const std::exception&)
		{
		}
	}
	default: return QVariant();
	}
}

Qt::ItemFlags QFindListModel::flags(const QModelIndex & index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

void QFindListModel::setValue(int value)
{
	firstIndex = value - 100;
	if (firstIndex < 0)
		firstIndex = 0;

	lastIndex = value + 100;
	if (lastIndex > offsetsMap.size())
		lastIndex = offsetsMap.size();

	emit findCount(QString::fromStdString(std::to_string(offsetsMap.size())));
}