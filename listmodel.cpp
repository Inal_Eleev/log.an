#include "listmodel.h"
#include <boost/thread/future.hpp>

QListModel::QListModel(QString fileName, QObject *parent)
	: parent_(parent)
	, fileName_(fileName)
	, fileSize_(0)
	, lastIndex_(MIN_LINES_VIEW)
	, firstIndex_(0)
	, lines_(0)
	, rows_(MIN_LINES_VIEW)
	, work_(new work_t(ioService_))
	, serviceThread_(boost::bind(&boost::asio::io_service::run, &ioService_))
{ 
	fileq_ = new QFile(fileName_);
	if (!fileq_->open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	fileSize_ = fileq_->size();
}

QListModel::~QListModel()
{
	fileq_->close();

	work_.reset();
	serviceThread_.join();
}

void QListModel::InitFile()
{
	ioService_.post([this]()
	{
		auto readFile = [this](qint64 begin, qint64 end, bool progCheck = false) -> QList<qint64>
		{
			QList<qint64> offsetList;
			std::ifstream instream(fileName_.toStdString(), std::ifstream::binary);
			std::string line;
			if (instream.is_open())
			{
				if (begin > 0)
				{
					instream.seekg(begin);
					std::getline(instream, line);
				}
				qint64 offset_per_line = 0;
				int progress = 0;
				while (std::getline(instream, line))
				{
					qint64 pos = instream.tellg();
					offsetList.append(offset_per_line);

					{
						// This mutex is really important, couse of the threadrace
						boost::lock_guard<boost::recursive_mutex> lock(linesGuard_);
						++lines_;
					}
					offset_per_line = pos;

					if (progCheck)
					{
						int temp = ((offset_per_line * 92) / end);
						if (temp > progress)
						{
							emit progressRead(temp);
							progress = temp;
						}
					}

					if (pos > end)
						break;
				}

				instream.close();
			}

			return offsetList;
		};

		const qint64 delim = fileSize_ / 8;
		auto f1 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(0, delim, true); });
		auto f2 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(delim, delim * 2); });
		auto f3 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(delim * 2, delim * 3); });
		auto f4 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(delim * 3, delim * 4); });
		auto f5 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(delim * 4, delim * 5); });
		auto f6 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(delim * 5, delim * 6); });
		auto f7 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(delim * 6, delim * 7); });
		auto f8 = boost::async([&readFile, delim]() -> QList<qint64> { return readFile(delim * 7, delim * 8); });

		{
			//boost::lock_guard<boost::recursive_mutex> lock(offsetListGuard_);
			offsetList_.append(f1.get());
			offsetList_.append(f2.get()); emit progressRead(93);
			offsetList_.append(f3.get()); emit progressRead(94);
			offsetList_.append(f4.get()); emit progressRead(95);
			offsetList_.append(f5.get()); emit progressRead(96);
			offsetList_.append(f6.get()); emit progressRead(97);
			offsetList_.append(f7.get()); emit progressRead(98);
			offsetList_.append(f8.get()); emit progressRead(99);
		}

		if (lines_ > INT32_MAX)
			lines_ = INT32_MAX;

		boost::lock_guard<boost::recursive_mutex> lock(linesGuard_);
		beginInsertRows(QModelIndex(), rows_, lines_);
		rows_ = lines_;
		endInsertRows();

		emit progressRead(100);
	});
}

int QListModel::rowCount(const QModelIndex &/*parent*/) const
{
	if (lines_ > INT32_MAX)
		return INT32_MAX;

	return lines_;
}

QVariant QListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid() || !(index.row() >= firstIndex_ && index.row() <= lastIndex_))
		return QVariant();

	switch (role)
	{
	case Qt::EditRole:
	case Qt::DisplayRole :
	{
		qint64 it = 0;
		try
		{
			boost::lock_guard<boost::recursive_mutex> lock(offsetListGuard_);
			it = offsetList_.at(index.row());
		}
		catch (const std::exception&)
		{
			return QVariant();
		}

		fileq_->seek(it);
		QTextStream in(fileq_);
		return QString::number(index.row()) + "	" + in.readLine();
	}
	default: return QVariant();
	}
}

QVariant QListModel::GetDataByLine(const int index)
{
	qint64 it = 0;
	try
	{
		boost::lock_guard<boost::recursive_mutex> lock(offsetListGuard_);
		it = offsetList_.at(index);
	}
	catch (const std::exception&)
	{
		return QVariant();
	}

	fileq_->seek(it);
	QTextStream in(fileq_);
	return in.readLine();
}

Qt::ItemFlags QListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

void QListModel::fetchMore(const QModelIndex & /*parent*/)
{
	if (lines_ > rows_)
	{
		beginInsertRows(QModelIndex(), rows_, lines_);
		rows_ = lines_;
		endInsertRows();
	}
}

bool QListModel::canFetchMore(const QModelIndex & /*parent*/) const
{
	boost::lock_guard<boost::recursive_mutex> lock(offsetListGuard_);
	return rows_ != offsetList_.size();
}

void QListModel::FindTextAction(QString findText)
{
	auto find = [this, findText](int begin, int end) -> bool
	{
		try
		{
			auto file = new QFile(fileName_);
			if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
				return false;

			qint64 curPosition = 0;
			for (auto i = begin; i < end; ++i)
			{
				curPosition = offsetList_.at(i);
				file->seek(curPosition);
				QTextStream in(file);
				if (in.readLine().contains(findText))
					emit findMatch(i);
			}
			return true;
		}
		catch (const std::exception&)
		{
		}
		return false;
	};

	const auto delim = (offsetList_.count() / 8) + 1;

	auto f1 = boost::async([&find, &delim]() { find(0, delim); });
	auto f2 = boost::async([&find, &delim]() { find(delim, delim * 2); });
	auto f3 = boost::async([&find, &delim]() { find(delim * 2, delim * 3); });
	auto f4 = boost::async([&find, &delim]() { find(delim * 3, delim * 4); });
	auto f5 = boost::async([&find, &delim]() { find(delim * 4, delim * 5); });
	auto f6 = boost::async([&find, &delim]() { find(delim * 5, delim * 6); });
	auto f7 = boost::async([&find, &delim]() { find(delim * 6, delim * 7); });
	auto f8 = boost::async([&find, &delim, r = offsetList_.size()]() { find(delim * 7, r); });

	f1.get();
	f2.get();
	f3.get();
	f4.get();
	f5.get();
	f6.get();
	f7.get();
	f8.get();

	emit findFinished();
}

void QListModel::setValue(int value)
{
	firstIndex_ = value - MIN_LINES_VIEW;
	if (firstIndex_ < 0)
		firstIndex_ = 0;

	lastIndex_ = value + MIN_LINES_VIEW;
	if (lastIndex_ > lines_)
		lastIndex_ = lines_;
}

