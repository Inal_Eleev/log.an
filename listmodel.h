#ifndef LISTMODEL_H
#define LISTMODEL_H

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/scoped_ptr.hpp>

#include <fstream>
#include <iostream>
#include <set>

#include <QAbstractListModel>
#include <QFile>
#include <QTextStream>
#include <QModelIndex>
#include <QList>
#include <QMap>
#include <QProgressBar>

namespace  {
   const uint16_t MIN_LINES_VIEW = 200;
}
class QListModel : public QAbstractListModel
{
	Q_OBJECT
public:
	explicit QListModel(QString fileName, QObject* parent = nullptr);
	~QListModel() override;

	void InitFile();

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	QVariant GetDataByLine(const int index);

	Qt::ItemFlags flags(const QModelIndex& index) const override;

	void fetchMore(const QModelIndex &parent) override;
	bool canFetchMore(const QModelIndex &parent) const override;

	qint64 GetLines() const { return lines_; }

	void FindTextAction(QString findText);
	QFile* GetFilePointer() const { return fileq_; }
private:

private:
	QObject* parent_;
	QString fileName_;
	QFile* fileq_;
	QList<qint64> offsetList_;
	qint64 fileSize_;

	qint64 lastIndex_;
	qint64 firstIndex_;
	qint64 lines_;
	qint64 rows_;

	using work_t = boost::asio::io_service::work;
	boost::asio::io_service						ioService_;
	boost::scoped_ptr<work_t>					work_;
	boost::thread								serviceThread_;
	mutable boost::recursive_mutex				offsetListGuard_;
	mutable boost::recursive_mutex				linesGuard_;

public slots:
	void setValue(int value);

public: signals:
	void progressRead(int value);
	void findMatch(int line);
	void findFinished();
};

#endif // LISTMODEL_H
