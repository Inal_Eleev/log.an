#include "itemSelectionModel.h"

LogItemSelectionModel::LogItemSelectionModel(QAbstractItemModel * model)
	: QItemSelectionModel(model)
{
}

LogItemSelectionModel::LogItemSelectionModel(QAbstractItemModel * model, QObject * parent)
	: QItemSelectionModel(model, parent)
{
}

LogItemSelectionModel::~LogItemSelectionModel()
{
}
