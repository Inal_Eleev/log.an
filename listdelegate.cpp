#include "listdelegate.h"

ListDelegate::ListDelegate(QObject* parent)
	: m_parent(parent)
{
}

ListDelegate::~ListDelegate()
{
}

void ListDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	QStyledItemDelegate::paint(painter, option, index);
}


QSize ListDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	return QStyledItemDelegate::sizeHint(option, index);
}

QWidget * ListDelegate::createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	return QStyledItemDelegate::createEditor(parent, option, index);
}

void ListDelegate::setEditorData(QWidget * editor, const QModelIndex & index) const
{
	QStyledItemDelegate::setEditorData(editor, index);
}

void ListDelegate::setModelData(QWidget * editor, QAbstractItemModel * model, const QModelIndex & index) const
{
	QStyledItemDelegate::setModelData(editor, model, index);
}

void ListDelegate::updateEditorGeometry(QWidget * editor, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	QStyledItemDelegate::updateEditorGeometry(editor, option, index);
}

QString ListDelegate::displayText(const QVariant & value, const QLocale & locale) const
{
	return QStyledItemDelegate::displayText(value, locale);
}

void ListDelegate::initStyleOption(QStyleOptionViewItem * option, const QModelIndex & index) const
{
	QStyledItemDelegate::initStyleOption(option, index);
}

bool ListDelegate::eventFilter(QObject * object, QEvent * event)
{
	return QStyledItemDelegate::eventFilter(object, event);
}

bool ListDelegate::editorEvent(QEvent * event, QAbstractItemModel * model, const QStyleOptionViewItem & option, const QModelIndex & index)
{
	return QStyledItemDelegate::editorEvent(event, model, option, index);
}
