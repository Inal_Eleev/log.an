#ifndef FINDMODEL_H
#define FINDMODEL_H

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/scoped_ptr.hpp>

#include <fstream>
#include <iostream>
#include <set>

#include <QAbstractListModel>
#include <QFile>
#include <QTextStream>
#include <QModelIndex>
#include <QList>
#include <QProgressBar>

#include "listmodel.h"

class QFindListModel : public QAbstractListModel
{
	Q_OBJECT
public:
	explicit QFindListModel(QFile* file, QListModel* parent = nullptr);
	~QFindListModel() override;


	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

public slots:
	void setValue(int value);
	void InitFindMap(int line);
	void findFinished();

public: signals:
	void findCount(QString value);

private:
	QListModel* m_parent;
	QFile* m_fileq;
	mutable QList<int> offsetsMap;

	mutable bool is_sorted;

	int32_t lastIndex;
	int32_t firstIndex;
	int32_t rows;
	mutable boost::recursive_mutex m_mapGuard;
};

#endif // FINDMODEL_H