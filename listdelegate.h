#ifndef LIST_DELEGATE_H
#define LIST_DELEGATE_H

#include <QStyledItemDelegate>

class ListDelegate : public QStyledItemDelegate
{
	Q_OBJECT
public:
	explicit ListDelegate(QObject* parent = nullptr);
	~ListDelegate();
	/// painting ///
	/// ������������ ������ ��������
	void paint(QPainter *painter,
		const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	QSize sizeHint(const QStyleOptionViewItem &option,
		const QModelIndex &index) const override;

	/// editing ///
	/// ������� ������ ��������� ��������
	QWidget *createEditor(QWidget *parent,
		const QStyleOptionViewItem &option,
		const QModelIndex &index) const override;

	/// �������� �������� �� ������ � ��������
	void setEditorData(QWidget *editor, const QModelIndex &index) const override;

	/// �������� �������� �� ��������� � ������
	void setModelData(QWidget *editor,
		QAbstractItemModel *model,
		const QModelIndex &index) const override;

	/// ������ �������� ���������
	void updateEditorGeometry(QWidget *editor,
		const QStyleOptionViewItem &option,
		const QModelIndex &index) const override;

	virtual QString displayText(const QVariant &value, const QLocale &locale) const;

protected:
	void initStyleOption(QStyleOptionViewItem *option,
		const QModelIndex &index) const override;

	bool eventFilter(QObject *object, QEvent *event) override;
	bool editorEvent(QEvent *event, QAbstractItemModel *model,
		const QStyleOptionViewItem &option, const QModelIndex &index) override;

private:
	QObject* m_parent;
};

#endif // LIST_DELEGATE_H