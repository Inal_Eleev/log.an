/********************************************************************************
** Form generated from reading UI file 'log_an.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOG_AN_H
#define UI_LOG_AN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Log_An
{
public:
    QAction *action_open;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QSplitter *splitter;
    QListView *listView;
    QListView *listView_2;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QProgressBar *progressBar;
    QSpacerItem *horizontalSpacer;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Log_An)
    {
        if (Log_An->objectName().isEmpty())
            Log_An->setObjectName(QString::fromUtf8("Log_An"));
        Log_An->resize(1453, 1033);
        action_open = new QAction(Log_An);
        action_open->setObjectName(QString::fromUtf8("action_open"));
        action_open->setCheckable(true);
        action_open->setShortcutVisibleInContextMenu(true);
        centralWidget = new QWidget(Log_An);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        listView = new QListView(splitter);
        listView->setObjectName(QString::fromUtf8("listView"));
        listView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        listView->setMovement(QListView::Static);
        listView->setProperty("isWrapping", QVariant(false));
        splitter->addWidget(listView);
        listView_2 = new QListView(splitter);
        listView_2->setObjectName(QString::fromUtf8("listView_2"));
        splitter->addWidget(listView_2);

        gridLayout->addWidget(splitter, 0, 0, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy);
        progressBar->setValue(0);

        horizontalLayout->addWidget(progressBar);

        horizontalSpacer = new QSpacerItem(398, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        Log_An->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Log_An);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1453, 21));
        Log_An->setMenuBar(menuBar);
        statusBar = new QStatusBar(Log_An);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Log_An->setStatusBar(statusBar);

        retranslateUi(Log_An);

        QMetaObject::connectSlotsByName(Log_An);
    } // setupUi

    void retranslateUi(QMainWindow *Log_An)
    {
        Log_An->setWindowTitle(QApplication::translate("Log_An", "Log.An", nullptr));
        action_open->setText(QApplication::translate("Log_An", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214", nullptr));
#ifndef QT_NO_SHORTCUT
        action_open->setShortcut(QApplication::translate("Log_An", "Ctrl+O", nullptr));
#endif // QT_NO_SHORTCUT
        label->setText(QApplication::translate("Log_An", "This is text label for example", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Log_An: public Ui_Log_An {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOG_AN_H
