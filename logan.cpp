#include "logan.h"

#include "ui_log_an.h"

#include "itemSelectionModel.h"

Logan::Logan(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::Log_An)
	, m_model(nullptr)
{
	ui->setupUi(this);

	open = new QAction(tr("&Open"), this);
	find = new QAction(tr("&Find"), this);

	open->setShortcuts(QKeySequence::New);
	find->setShortcuts(QKeySequence::Find);

	open->setStatusTip(tr("Open"));
	find->setStatusTip(tr("Find"));

	connect(open, &QAction::triggered, this, &Logan::on_actionOpen_triggered);
	connect(find, &QAction::triggered, this, &Logan::on_actionFind_triggered);

	ui->menuBar->addAction(open);
	ui->menuBar->addAction(find);

	ui->listView->verticalScrollBar()->setMinimum(0);
	ui->listView->horizontalScrollBar()->show();

	dialog = new FindDialog;

	ListDelegate* del = new ListDelegate(this);
	ui->listView->setItemDelegate(del);

	ui->listView_2->setItemDelegate(del);
}

void Logan::on_actionOpen_triggered()
{
	QString fileName = QFileDialog::getOpenFileName(this, "Open the file");
	if (fileName.isEmpty())
		return;

	if (m_model)
		delete m_model;

	m_model = new QListModel(fileName, this);

	ui->listView->setSelectionModel(new LogItemSelectionModel(m_model));
	ui->listView->setSelectionMode(QAbstractItemView::ExtendedSelection);

	connect(m_model, &QListModel::progressRead, ui->progressBar, &QProgressBar::setValue);

	m_model->InitFile();

	ui->listView->setModel(m_model);
	connect(ui->listView->verticalScrollBar(), &QScrollBar::valueChanged, m_model, &QListModel::setValue);
}

Logan::~Logan()
{
	delete ui;
}

void Logan::on_actionFind_triggered()
{
	dialog->show();

	if (dialog->exec() == QDialog::Accepted)
	{
		const auto findText = dialog->getFindText();
		if (findText.isEmpty())
			return;

		auto findModel = new QFindListModel(m_model->GetFilePointer(), m_model);
		connect(m_model, &QListModel::findMatch, findModel, &QFindListModel::InitFindMap);
		connect(m_model, &QListModel::findFinished, findModel, &QFindListModel::findFinished);

		m_model->FindTextAction(findText);

		ui->listView_2->setModel(findModel);
		connect(ui->listView_2->verticalScrollBar(), &QScrollBar::valueChanged, findModel, &QFindListModel::setValue);

		connect(findModel, &QFindListModel::findCount, ui->label, &QLabel::setText);
	}
}
