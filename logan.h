#ifndef LOGAN_H
#define LOGAN_H

#include "listdelegate.h"
#include "listmodel.h"
#include "finddialog.h"
#include "findmodel.h"

#include <QMainWindow>
#include <QtWidgets>

namespace Ui {
class Log_An;
}

class Logan : public QMainWindow
{
	Q_OBJECT

public:
	explicit Logan(QWidget *parent = nullptr);
	~Logan();

private:

private slots:
	void on_actionOpen_triggered();
	void on_actionFind_triggered();

private:
	Ui::Log_An *ui;
	QAction* open;
	QAction* find;
	QListModel* m_model;
	FindDialog *dialog;
};

#endif // LOGAN_H
